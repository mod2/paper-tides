\documentclass[11pt,letterpaper,twoside]{report}


	\usepackage{fullpage}
	\usepackage{graphicx}
	\usepackage[numbers]{natbib}
	\usepackage[linktoc=all,backref]{hyperref}
	\usepackage{sectsty}
	\usepackage[raggedright]{titlesec}
	\usepackage[font=small,labelfont=bf,justification=centering]{caption}
	\usepackage{listings}
	\usepackage{color}
	\usepackage{alltt}
	\usepackage{bm}
	\usepackage{multicol}

	\renewcommand*\rmdefault{ptm}
	\renewcommand*\bibname{Literature Cited}
	\setcounter{tocdepth}{3}
	\setcounter{secnumdepth}{3}
	\setlength{\columnseprule}{1pt}
	\setlength{\columnsep}{13pt}

	\newcommand*{\ParFlow}{\textsc{ParFlow}}
	\newcommand*{\MODFLOW}{\textsc{modflow}}
	\newcommand*{\mfnstotk}{\textsc{mf96to2k}}
	\newcommand*{\mftktoof}{\textsc{mf2kto05}}
	\newcommand*{\FloPy}{\textsc{FloPy}}
	\newcommand*{\FloPerl}{\textsc{FloPerl}}
	\newcommand*{\Tcl}{\textsc{Tcl}}

	\titleformat{\chapter}[display]
		{\centering\huge\bfseries}{\thechapter}{1em}{\huge}
	\chapterfont{\centering}
	\raggedbottom
%	\widowpenalties 1 10000


\begin{document}


	\pagenumbering{arabic}

	{\huge\bfseries From \MODFLOW{}-96 to \MODFLOW{}-2005, \ParFlow{}, and Others:\par}
	{\LARGE\bfseries Updates and a Workflow for Up- and
		Out-Conversion\footnote{
			\footnotesize\centering
			\setlength{\parskip}{0.5\baselineskip}
			Copyright \copyright 2017, Daniel Hardesty~Lewis\par
			All rights reserved.\par
			Redistribution and use of this computer code in source and binary
				forms, with or without modification, are permitted provided
				that the conditions of the {M}odified~{BSD}~{L}icense are
				met.\par
			Permission is granted to copy, distribute, and\slash or modify this
				document under the terms of the {C}reative~{C}ommons
				{A}ttribution 4.0 {I}nternational {P}ublic~{L}icense.
		}\par}
	{\Large\itshape Daniel Hardesty~Lewis\par}

	\begin{multicols}{2}
		\raggedright

		\paragraph*{}
		Despite supersession by a number of its own versions, as well as other,
			newer groundwater modelling softwares, \MODFLOW{}-96 and
			its legacy simulations remain as \textit{de facto} standards within
			much of the hydrogeological community within the {S}tate of {T}exas
			and elsewhere.
		Unfortunately, even the existence of conversion utilities amongst the
			differing versions of \MODFLOW{} \cite{harbaugh96} has not
			necessarily stimulated the adoption of the more modern versions,
			let alone the re-creation of equivalent models from them.
		The primary reason for such a lack of adoption stands as the
			unfamiliarity of the modeller with command line or the Fortran
			programming language, resulting in an inability to address either
			the minor or major bugs, nuances, or limitations in compilation or
			execution of the conversion programs \cite{ansi78}.
		Here, we present a workflow which captures the above intricacies
			all the while attempting to maintain a great portability in
			implementation.\footnote{The work, including this paper, discussed
			hereafter is recorded by the Git projects grouped under the
			following:\par
			\url{https://gitlab.com/mod2}}
	
		\paragraph*{}
		This workflow is constructed in the form of a Bash script and -- with
			those more oriented towards the geosciences in mind -- re-presented
			as a Jupyter notebook \cite{gnu16} \cite{jupyter17}.
		One may choose whether the program will operate with compliance with
			the Portable Operating System Interface standards or with a
			preference towards Bash facilities \cite{austin16}.
		Both strongly extend the re-usability of this program as both are
			widely adopted amongst many UNIX and other operating systems.
		In the same vein, attempts are made to function within environments not
			possessing the standard commands, which reduces the required
			dependencies.
		As well, it is designed to offer parallelisation across as many cores
			and nodes as necessary or as few as desired, whether upon a
			personal or super-computer.
	
	\begin{figure*}
		\centering
		\begin{minipage}{0.45\textwidth}
			\centering
			\includegraphics[width=0.8\textwidth]{Peckham-Point_Point.png}
			\caption{$O(n^2)$ -- an exemplary, fully connected
				point-to-point network}
			\label{fig:p2p}
		\end{minipage}\hfill
		\begin{minipage}{0.45\textwidth}
			\centering
			\includegraphics[width=0.9\textwidth]{Peckham-Spoke_Hub.png}
			\caption{$O(n)$ -- a spoke-hub network with a single central
				node}
			\label{fig:sh}
		\end{minipage}
	\end{figure*}
	\paragraph*{}
		Underlying this workflow are some programs in which updates have been
			written such that they may compile and execute upon modern
			hardware.
		Also, fixes to long-standing bugs and limitations in the existing
			\MODFLOW{} conversion utilities have been prepared.
		Specifically, support for the up-conversion of -96 simulations which
			were run in conjunction with the {H}orizontal~{F}low {B}arrier
			package has been added \cite{hsieh93}.
		More radically, the foundations of a conversion utility between
			\MODFLOW{} and the groundwater modelling software, \ParFlow{}, have
			been set \cite{maxwell16}.
		Even further, the modular approach followed in crafting this converter
			is such that it may extend to a general-purpose application which
			interoperates between arbitrary groundwater modellers.
		In particular, the spoke ontology, {G}eoscience {S}tandard~{N}ames, was
			targetted by each converter \cite{peckham14}.\footnote{The
			illustrations on the following page display both the improvement in
			portability and the reduction in complexity such a model promotes.}

		\paragraph*{}
		In short, an accessible and portable workflow of the process of
			up-conversion between \MODFLOW{} versions now avails itself to
			geoscientists.
		Some components of it may find applicability in the re-use of legacy
			simulations.
		Lastly, a generic inter-operator has been established, invoking the
			possibility of significant ease in the recycling of groundwater
			data in the future.
	
	\end{multicols}

	\let\Origclearpage\clearpage
	\let\clearpage\relax
	\bibliographystyle{plainnat}
	\bibliography{./sds-csc-paper.bib}
	\let\clearpage\Origclearpage


\end{document}
