\contentsline {chapter}{Literature Cited}{2}{chapter*.2}
\contentsline {chapter}{Acknowledgements}{4}{chapter*.3}
\contentsline {paragraph}{}{4}{chapter*.3}
\contentsline {paragraph}{}{4}{chapter*.3}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Materials and Methods}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Failings with \textsc {FloPerl}{}}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Towards Efficient Conversion Input/Output}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}A Step-Back -- Limitations in the Original Converters}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}A Workflow -- Up-Conversion of Legacy Models}{8}{section.2.4}
\contentsline {chapter}{\numberline {3}Results}{9}{chapter.3}
\contentsline {chapter}{\numberline {4}Discussion}{10}{chapter.4}
\contentsline {chapter}{Literature Cited}{12}{chapter*.6}
\contentsline {chapter}{Reflection}{14}{chapter*.7}
\contentsline {chapter}{Appendices}{15}{section*.8}
\contentsline {chapter}{\numberline {A}Computer Code}{16}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}\textsc {modflow}{} Up-Conversion}{16}{section.1.A.1}
\contentsline {subsection}{\numberline {A.1.1}Workflow}{16}{subsection.1.A.1.1}
\contentsline {subsubsection}{\numberline {A.1.1.1}ZIP Archive Unnesting}{18}{subsubsection.1.A.1.1.1}
\contentsline {subsubsection}{\numberline {A.1.1.2}Up-Conversion and Simulation}{19}{subsubsection.1.A.1.1.2}
\contentsline {subsection}{\numberline {A.1.2}Patches}{20}{subsection.1.A.1.2}
\contentsline {subsubsection}{\numberline {A.1.2.1}\textsc {modflow}{}-96 to -2000 -- Without HFB}{20}{subsubsection.1.A.1.2.1}
\contentsline {subsubsection}{\numberline {A.1.2.2}\textsc {modflow}{}-96 to -2000 -- With HFB}{26}{subsubsection.1.A.1.2.2}
\contentsline {subsubsection}{\numberline {A.1.2.3}\textsc {modflow}{}-2000 to -2005}{38}{subsubsection.1.A.1.2.3}
\contentsline {section}{\numberline {A.2}\textsc {modflow}{}-2005 to \textsc {ParFlow}{} Conversion}{39}{section.1.A.2}
\contentsline {subsection}{\numberline {A.2.1}\textsc {modflow}{}-2005 Input Modules}{39}{subsection.1.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}\textsc {modflow}{}-2005 to \textsc {ParFlow}{} Crosswalk}{45}{subsection.1.A.2.2}
\contentsline {subsubsection}{\numberline {A.2.2.1}TF-IDF and Cosine Similarity Module}{46}{subsubsection.1.A.2.2.1}
\contentsline {subsection}{\numberline {A.2.3}\textsc {ParFlow}{} Output Modules}{48}{subsection.1.A.2.3}
\contentsline {subsection}{\numberline {A.2.4}\textsc {FloPerl}{}}{71}{subsection.1.A.2.4}
\contentsline {subsubsection}{\numberline {A.2.4.1}Two-Dimensional Integer Array Reader}{81}{subsubsection.1.A.2.4.1}
\contentsline {subsubsection}{\numberline {A.2.4.2}Real Array Reader}{84}{subsubsection.1.A.2.4.2}
\contentsline {subsubsection}{\numberline {A.2.4.3}Miscellaneous Subroutines}{87}{subsubsection.1.A.2.4.3}
\contentsline {chapter}{\numberline {B}Licenses}{89}{Appendix.1.B}
\contentsline {section}{\numberline {B.1}{M}odified\nobreakspace {}{BSD}\nobreakspace {}{L}icense}{89}{section.1.B.1}
\contentsline {section}{\numberline {B.2}{C}reative\nobreakspace {}{C}ommons Attribution 4.0 International {P}ublic\nobreakspace {}{L}icense}{89}{section.1.B.2}
